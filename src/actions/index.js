import * as types from './ActionTypes';

export function changeOption(option){
    return{
        type : types.CHANGE_OPTION,
        option,
    }
}

export function changePosition(lat, lng) {
    return{
        type : types.CHANGE_POSITION,
        position: {lat, lng},
        // lists???/?
    };
}
