import React, { Component } from 'react';
import Script from 'react-load-script';
import '../App.css';
import { connect } from 'react-redux';
import * as actions from '../actions';


class ShowMap_ extends Component {
  
  initMap = () => {
    let clickedPosition = {lat:37.532600, lng:127.024612};

    this.map = new window.google.maps.Map(document.getElementById('map'), {
      center: this.props.position,
      zoom: 15
    });

    let mapp = this.map;
    let markers = [];
    let infowindow = new window.google.maps.InfoWindow();
    
    function createMarker(place) {
      let placeLoc = place.geometry.location;
      let marker = new window.google.maps.Marker({
        map: mapp,
        position: place.geometry.location,
      });
      markers.push(marker);

      window.google.maps.event.addListener(marker, 'click', function() {

        markers = [];
        if (place.opening_hours!=undefined){
          let info = place.name + <br/> + ((place.opening_hours.open_now) ? "now open!" : "closed..") + place.formatted_phone_number; // open_now boolean도 있음!!!
          infowindow.setContent(info);
          infowindow.open(this.map, this);
        }
        else {
          let info = place.name + <br/> + place.formatted_phone_number;
          infowindow.setContent(info);
          infowindow.open(this.map, this);
        }
        
      });

    }

    this.map.addListener('click', (event)=>{ 
    //   this.marker = new window.google.maps.Marker({
    //     position:event.latLng,
    //     map:this.map,
    // });
      clickedPosition = event.latLng;
      this.service = new window.google.maps.places.PlacesService(this.map);
      for(let i=0;i<markers.length;i++){
        markers[i].setMap(null);
      }
      this.service.nearbySearch({
          location: clickedPosition,
          radius: 800,
          type: [('dentist')],
        },
        (results, status) => {
          if (status == window.google.maps.places.PlacesServiceStatus.OK) {
            for (let i = 0; i < results.length; i++) {
              createMarker(results[i]);
            }
            
          }
        }
      );
    });
    this.props.handlePosition(this.props.position.lat, this.props.position.lan);
  }

  constructor(props){
    super(props);
    this.map = undefined;
    this.marker = undefined;
    this.position = undefined;
    this.service = undefined;
    this.infowindow = undefined;
  }

  render() {
    return (
      <div>
        <div className = "map">    
        <h1></h1>
        <div id="map"></div>
        </div>
        <Script
          url="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqLBY9GPGKUS-dSxwKBZZ_7dFbpz-3rpA&libraries=places&callback=initMap" 
          onLoad={this.initMap.bind(this)}  
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  position:state.targetPosition.position,
});

const mapDispatchToProps = (dispatch) => ({
  handlePosition: (lat, lng)=>{dispatch(actions.changePosition(lat, lng))},
});

const ShowMap = connect(mapStateToProps, mapDispatchToProps)(ShowMap_);

export default ShowMap;
