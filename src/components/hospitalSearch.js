import React, { Component } from 'react';
import ShowMap from './showMap.js';
import HospitalList from './hospitalList.js';
import SelectHospital from './selectHospital.js';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { changePosition } from '../actions/index';
import manageLists from '../reducers/manageLists.js';

class HospitalSearch_ extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div>    
        <SelectHospital/>
        <ShowMap/>
        <HospitalList
          listToPrint = {this.props.listToPrint}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) =>  ({
  listToPrint: (state.hospitalOption.option == 1) 
  ? state.manageLists.animalList : ((state.hospitalOption.option == 2) 
  ? state.manageLists.surgeryList : ((state.hospitalOption.option == 3) 
  ? state.manageLists.internalList : ((state.hospitalOption.option == 4) 
  ? state.manageLists.dentalList : []))),
});

const mapDispatchToProps = (dispatch) => ({

  handlePosition: (lat, lng)=>{dispatch(actions.changePosition(lat, lng))},
});

const HospitalSearch = connect(mapStateToProps, mapDispatchToProps)(HospitalSearch_);

export default HospitalSearch;
