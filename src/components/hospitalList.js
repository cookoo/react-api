import React, { Component } from 'react';
import '../App.css';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

class HospitalList extends Component {
   
  showList = (selectedOne, index) => {
    return(
      <ListItem alignItems="flex-start">
        <ListItemText
          primary={selectedOne.name}
          secondary={
            <React.Fragment>
              <Typography component="span" color="textPrimary">
                {selectedOne.number}
              </Typography>
              {selectedOne.time}
            </React.Fragment>
          }
        />
      </ListItem>
    );
  } 



  render() {
    return (
      <div>
      <List >
      {this.props.listToPrint.map((a, index) => this.showList(a, index))}
      </List>
 
        </div>
    );
  }
}

export default HospitalList;
