import React, { Component } from 'react';
import '../App.css';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'; 
import * as actions from '../actions';
import { changeOption } from '../actions/index'

const styles = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
    extendedIcon: {
      marginRight: theme.spacing.unit,
    },
});
  
class SelectHospital extends Component {
  render() {
    return (
        <div>
            <Button 
              variant="contained"
              size="large"
              color="primary"
              onClick={() => this.props.handleOption(1)}
            >
            동물
            </Button>
            <Button 
              variant="contained" 
              size="large" 
              color="primary"
              onClick={() => this.props.handleOption(2)}
            >
            외과
            </Button>
            <Button 
              variant="contained" 
              size="large" 
              color="primary"
              onClick={() => this.props.handleOption(3)}
            >
            내과
            </Button>
            <Button 
              variant="contained" 
              size="large" 
              color="primary"
              onClick={() => this.props.handleOption(4)}
            >
            치과
            </Button>

        </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  handleOption: (option)=>{dispatch(actions.changeOption(option))},
})

export default connect(null, mapDispatchToProps)(SelectHospital);
