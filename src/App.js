import React, { Component } from 'react';
import './App.css';
import HospitalSearch from './components/hospitalSearch.js';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers';

const store = createStore(reducers);

class App extends Component {

  constructor(props){
    super(props);
  }


  render() {
    return (
      <div>    
        <Provider store = {store}>
         <HospitalSearch/>
        </Provider>
      </div>
    );
  }
}

export default App;
