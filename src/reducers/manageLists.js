import * as types from '../actions/ActionTypes';

const initialState = {
    animalList: 
    [
        {
            name:"anm",
            number:"010--",
            time: "sdas",
        },        
        {
            name:"anm2",
            number:"010--",
            time: "sdas",
        },
    ],
    surgeryList: [
        {
            name:"sur",
            number:"010--",
            time: "sdas",
        },        
        {
            name:"sur2",
            number:"010--",
            time: "sdas",
        },
    ],
    internalList: [
        {
            name:"inter",
            number:"010--",
            time: "sdas",
        },        
        {
            name:"inter2",
            number:"010--",
            time: "sdas",
        },
    ],
    dentalList: [
        {
            name:"dent",
            number:"010--",
            time: "sdas",
        },        
        {
            name:"dent2",
            number:"010--",
            time: "sdas",
        },
    ],
};

function manageLists (state = initialState, action) {
    switch(action.type) {
        case types.CHANGE_POSITION:
            return { 
                ...state,  
            }; 
        default:
            return state;
    }
}

export default manageLists;
