import { combineReducers } from 'redux';
import hospitalOption from './hospitalOption';
import manageLists from './manageLists';
import targetPosition from './targetPosition';

const reducers = combineReducers({
    manageLists, hospitalOption, targetPosition,
});

export default reducers;
