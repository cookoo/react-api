import * as types from '../actions/ActionTypes';
/**
 * 1 : animal
 * 2 : surgery
 * 3 : internal
 * 4 : dental
 */
const initialState = {
    option : 0,
};

function hospitalOption (state = initialState, action) {
    switch(action.type) {
        case types.CHANGE_OPTION:
            return { 
                ...state,  
                option: action.option,
            };
        default:
            return state;
    }
}

export default hospitalOption;
