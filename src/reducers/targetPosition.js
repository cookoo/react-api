import * as types from '../actions/ActionTypes';

const initialState = {
    position: {
        lat:37.532600, 
        lng:127.024612
    },
};

function targetPosition (state = initialState, action) {
    switch(action.type) {
        case types.CHANGE_POSITION:
            return { 
                ...state,  
                position : {lat:action.lat, lng:action.lng},
            };
        default:
            return state;
    }
}

export default targetPosition;
